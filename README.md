# README #

A simple Christmas tree ornament PCB fab file. 

### Content  ###

This repository holds the CADSoft Eagle schematic and board files for creating a PCB Christmas tree ornament that will twinkle...

-------------------

### Version History  ###

* Version 1 - initial.

-------------------

### BOM  ###

1. Qty:3, LEDs 0805 package.  Designed for WL-SMCW Series LED (or any 0805 LED package - size R1 - R3 per its forward voltage and desired battery life).
2. Qty: 1, Link BAT-HLD-001, Coin cell battery holder.
3. Qty: 3, R1 - R3, Chip Resistor, 0805 package, 220 ohm (value to change based on LED selected and desired brightness / battery life).

Est. part Cost: $1.50 per assembly.

Boards Fab at OSH park cost $20 for 3.  Approximately $7 per board.

Total cost: ~ $8.50 U.S.D. per assembly sans shipping costs.

-------------------

### License Information ###

This product is _**open source**_! 

Hardware
---------

**This tree ornament hardware design is released under [Creative Commons Share-alike 4.0 International](http://creativecommons.org/licenses/by-sa/4.0/).**

Note: This is a human-readable summary of (and not a substitute for) the [license](http://creativecommons.org/licenses/by-sa/4.0/legalcode).

You are free to:

Share - copy and redistribute the material in any medium or format
Adapt - remix, transform, and build upon the material
for any purpose, even commercially.
The licensor cannot revoke these freedoms as long as you follow the license terms.
Under the following terms:

Attribution - You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
ShareAlike - If you remix, transform, or build upon the material, you must distribute your contributions under the same license as the original.
No additional restrictions - You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.
Notices:

You do not have to comply with the license for elements of the material in the public domain or where your use is permitted by an applicable exception or limitation.
No warranties are given. The license may not give you all of the permissions necessary for your intended use. For example, other rights such as publicity, privacy, or moral rights may limit how you use the material.